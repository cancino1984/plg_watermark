<?php
/**
 * @plugin      watermark
 * @copyright   Copyright (C) 2020 Oscar Cancino
 * @license     GNU/GPL
 * @website     http://www.enlacecorredores.cl/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

class plgSystemWatermark extends JPlugin {

    var $params;
    var $dirImages = 'plugins/system/watermark/images/';
    var $dirFont = 'plugins/system/watermark/font/Roboto-Black.ttf';

    function &getInstance()
    {
        static $instance = null;
        if (is_null($instance)) {
            $dispatcher = &JDispatcher::getInstance();
            $plugin     = &JPluginHelper::getPlugin('content', 'watermark');
            if (count($plugin)) {
                $instance = new plgSystemWatermark($dispatcher, (array) $plugin);
                $instance->params = new JParameter($plugin->params);
            }else{
                global $mainframe;
                $mainframe->enqueueMessage('Watermark Plugin is not enabled', 'error');
            }
        }
        return $instance;
    }
    
    function rgb2hex2rgb($color){ 
       if(!$color) return false; 
       $color = trim($color); 
       $result = false; 
      if(preg_match("/^[0-9ABCDEFabcdef#]+$/i", $color)){
          $hex = str_replace('#','', $color);
          if(!$hex) return false;
          if(strlen($hex) == 3):
             $result['r'] = hexdec(substr($hex,0,1).substr($hex,0,1));
             $result['g'] = hexdec(substr($hex,1,1).substr($hex,1,1));
             $result['b'] = hexdec(substr($hex,2,1).substr($hex,2,1));
          else:
             $result['r'] = hexdec(substr($hex,0,2));
             $result['g'] = hexdec(substr($hex,2,2));
             $result['b'] = hexdec(substr($hex,4,2));
          endif;       
       }elseif (preg_match("/^[0-9]+(,| |.)+[0-9]+(,| |.)+[0-9]+$/i", $color)){ 
          $rgbstr = str_replace(array(',',' ','.'), ':', $color); 
          $rgbarr = explode(":", $rgbstr);
          $result = '#';
          $result .= str_pad(dechex($rgbarr[0]), 2, "0", STR_PAD_LEFT);
          $result .= str_pad(dechex($rgbarr[1]), 2, "0", STR_PAD_LEFT);
          $result .= str_pad(dechex($rgbarr[2]), 2, "0", STR_PAD_LEFT);
          $result = strtoupper($result); 
       }else{
          $result = false;
       }

       return $result; 
    } 
    
    function getUrlImage($htmlContent){
        // read all image tags into an array
        preg_match_all('/<img[^>]+>/i',$htmlContent, $imgTags); 

        for ($i = 0; $i < count($imgTags[0]); $i++) {
          // get the source string
          preg_match('/src="([^"]+)/i',$imgTags[0][$i], $imgage);

          // remove opening 'src=' tag, can`t get the regex right
          $origImageSrc[] = str_ireplace( 'src="', '',  $imgage[0]);
        }
        // will output all your img src's within the html string
        return $origImageSrc;
    }
    
    function addTextWatermark($src, $watermark) {
        $info = new SplFileInfo($src);
        list($width, $height) = getimagesize($src);
        $image_color = imagecreatetruecolor($width, $height);
        $image = imagecreatefromjpeg($src);
        imagecopyresampled($image_color, $image, 0, 0, 0, 0, $width, $height, $width, $height);
        $colores = $this->rgb2hex2rgb($this->params->get('colorTextWatermark'));
        $txtcolor = imagecolorallocate($image_color, $colores['r'], $colores['g'], $colores['b']);
        
        $bbox = imagettfbbox($this->params->get('fontSizeWatermark'), $this->params->get('slantWatermark'), $this->dirFont, $watermark);
        
        $textWidth = ($width/2) - ($bbox[4] / 2);
        $textHeight = ($height/2) - ($bbox[3] / 2);
        imagettftext($image_color, $this->params->get('fontSizeWatermark'), $this->params->get('slantWatermark'), $textWidth, $textHeight, $txtcolor, $this->dirFont, $watermark);
        imagejpeg ($image_color, $this->dirImages.$info->getFilename(), 100); 
        imagedestroy($image); 
        imagedestroy($image_color);
        return $this->dirImages.$info->getFilename();
    }

    function addImageWatermark($SourceFile, $WaterMark, $opacity) {
        $info = new SplFileInfo($SourceFile);
        $main_img = $SourceFile; 
        $watermark_img = $WaterMark; 
        $padding = 5; 
        // crear marca de agua
        $watermark = imagecreatefrompng($watermark_img); 
        
        imageAlphaBlending($watermark, false);
        imageSaveAlpha($watermark, true);
        
        $fileType = $info->getextension();

        switch($fileType) {
            case('gif'):
                $image = imagecreatefromgif($main_img);
                break;

            case('png'):
                $image = imagecreatefrompng($main_img);
                break;

            default:
                $image = imagecreatefromjpeg($main_img);
        }
                
        if(!$image || !$watermark) die("Error: La imagen principal o la imagen de marca de agua no se pudo cargar!");
        
        $watermark_size = getimagesize($watermark_img);
        $watermark_width = $watermark_size[0]; 
        $watermark_height = $watermark_size[1]; 
        $image_size = getimagesize($main_img); 
        
        $dest_x = $image_size[0] - $watermark_width - $padding; 
        $dest_y = $image_size[1] - $watermark_height - $padding;
        imagecopymerge($image, $watermark, $dest_x, $dest_y, 0, 0, $watermark_width, $watermark_height, $opacity);
//        if ($DestinationFile<>'') {
           imagejpeg($image, $this->dirImages.$info->getFilename(), 100); 
//        } else {
//            header('Content-Type: image/jpeg');
//            imagejpeg($image);
//        }
        imagedestroy($image); 
        imagedestroy($watermark);
        return $this->dirImages.$info->getFilename();
    }
    
    function watermark($sourcefile, $watermarkfile) {

        #
        # $sourcefile = Filename of the picture to be watermarked.
        # $watermarkfile = Filename of the 24-bit PNG watermark file.
        #

        //Get the resource ids of the pictures
        $watermarkfile_id = imagecreatefrompng($watermarkfile);

        imageAlphaBlending($watermarkfile_id, false);
        imageSaveAlpha($watermarkfile_id, true);
        $info = new SplFileInfo($sourcefile);
        $fileType = $info->getextension();

        switch($fileType) {
            case('gif'):
                $sourcefile_id = imagecreatefromgif($sourcefile);
                break;

            case('png'):
                $sourcefile_id = imagecreatefrompng($sourcefile);
                break;

            default:
                $sourcefile_id = imagecreatefromjpeg($sourcefile);
        }

        //Get the sizes of both pix  
      $sourcefile_width=imageSX($sourcefile_id);
      $sourcefile_height=imageSY($sourcefile_id);
      $watermarkfile_width=imageSX($watermarkfile_id);
      $watermarkfile_height=imageSY($watermarkfile_id);

        $dest_x = ( $sourcefile_width / 2 ) - ( $watermarkfile_width / 2 );
        $dest_y = ( $sourcefile_height / 2 ) - ( $watermarkfile_height / 2 );

        // if a gif, we have to upsample it to a truecolor image
        if($fileType == 'gif') {
            // create an empty truecolor container
            $tempimage = imagecreatetruecolor($sourcefile_width,
                                                                                $sourcefile_height);

            // copy the 8-bit gif into the truecolor image
            imagecopy($tempimage, $sourcefile_id, 0, 0, 0, 0,
                                $sourcefile_width, $sourcefile_height);

            // copy the source_id int
            $sourcefile_id = $tempimage;
        }

        imagecopy($sourcefile_id, $watermarkfile_id, $dest_x, $dest_y, 0, 0, $watermarkfile_width, $watermarkfile_height);

        //Create a jpeg out of the modified picture
        switch($fileType) {

            // remember we don't need gif any more, so we use only png or jpeg.
            // See the upsaple code immediately above to see how we handle gifs
            case('png'):
//                header("Content-type: image/png");
                imagepng ($sourcefile_id, $this->dirImages.$info->getFilename(), 100);
                break;

            default:
                imagejpeg($sourcefile_id, $this->dirImages.$info->getFilename(), 100);
        }          

        imagedestroy($sourcefile_id);
        imagedestroy($watermarkfile_id);
        return $this->dirImages.$info->getFilename();

    }
    
    function onAfterDispatch()
    {
        
        $app = JFactory::getApplication();

        if ($app->isAdmin()) {
            return true;
        }

        $doc     = JFactory::getDocument();
        $content = $doc->getBuffer('component');
        
        $marcas = explode('id="'.$this->params->get('idWatermark').'"', $content);
        
        if(count($marcas)){
        
            for($i=1;$i<count($marcas);$i++) {

                    $urlImage = $this->getUrlImage($marcas[$i])[0];
                    
                    if($this->params->get('opWatermark') == 1 && $this->params->get('textWatermark')!=''){
                        $urlImgMark = $this->addTextWatermark($urlImage, $this->params->get('textWatermark'));
                        $content = str_ireplace($urlImage,$urlImgMark,$content); 
                    }
                    
                    if($this->params->get('opWatermark') == 0 && $this->params->get('imgWatermark')!=''){
                        $urlImgMark = $this->watermark($urlImage, $this->params->get('imgWatermark'));
                        $content = str_ireplace($urlImage,$urlImgMark,$content); 
                    }
                    
                       
            }

        }
        
        $doc->setBuffer($content, 'component');
        return true;
    }

}